# collector 를 실수로 장 마감 전에 돌렸을 경우 오늘 자 데이터를 삭제하기 위한 프로그램이다.
from library.daily_crawler import *


class Delete_day:
    def __init__(self):
        self.__creat_engine()

    def __creat_engine(self):
        self.engine_daily_craw = create_engine(
            "mysql+mysqldb://" + cf.db_id + ":" + cf.db_passwd + "@" + cf.db_ip + ":" + cf.db_port + "/daily_craw",
            encoding='utf-8')

        self.engine_daily_buy_list = create_engine(
            "mysql+mysqldb://" + cf.db_id + ":" + cf.db_passwd + "@" + cf.db_ip + ":" + cf.db_port + "/daily_buy_list",
            encoding='utf-8')

        self.engine_JB = create_engine(
            "mysql+mysqldb://" + cf.db_id + ":" + cf.db_passwd + "@" + cf.db_ip + ":" + cf.db_port + "/" + cf.imi1_db_name,
            encoding='utf-8')

    def item_tables(self):
        print("item_tables 함수를 실행합니다.")
        print("daily-craw 폴더 모든 종목 테이블 이름의 list를 불러옵니다.")
        sql="show tables in daily_craw"
        rows = self.engine_daily_craw.execute(sql).fetchall()
        print(rows)

        print("daily-craw 폴더 안에 지워야 할 데이터가 있는 종목 테이블 이름의 list를 불러옵니다.")
        self.rows=[]
        for i in rows:
            if '&' in i[0] or '-' in i[0] or ' ' in i[0] or '.' in i[0]:
                name="`%s`" %i[0]
            else:
                name=i[0]

            sql = "select count(date) from %s where date = %s"
            if self.engine_daily_craw.execute(sql % (name, self.date)).fetchall()[0][0]==1:
                self.rows.append((name,))
            print(name)
        print(self.rows)
        print("item_tables 함수를 완료했습니다.")

    def delete_day_info(self):
        print("delete_day_info 함수를 실행합니다.")
        # 실제 원하는 날 데이터 삭제 코드
        self.date = input("어떤 일자 데이터를 지우고 싶으신가요? (ex/20201231) : ")

        self.item_tables() # 해당 데이터가 있는 테이블 리스트를 만듭니다.

        # 나중에 코드가 완성되면 아래 코딩을 활성화 시킨다.
        print("daily_craw 폴더의 각 종목 테이블에 대하여, %s 에 해당하는 데이터 행을 삭제합니다." % self.date)
        for i in self.rows:
            print("%s 테이블의 %s 일자 정보를 삭제합니다." % (i[0], self.date))
            sql = "delete from daily_craw.%s where date = %s"
            self.engine_daily_craw.execute(sql % (i[0], self.date))

        print("삭제가 완료 되었습니다.")

    def update_indicator(self):
        print("update_indicator 함수에 들어왔습니다.")
        print("mysql//jackbot1_imi1.setting_data 에 각 종목별 업데이트 완료여부 표시를 미완료로 변경합니다.")
        sql="update setting_data set daily_crawler = %s"
        # 아래 str(~~) 부분을 지우고 내가 원하는 날짜를 직접 입력하는 것이 더 정확하다.
        # self.date 에 입력하는 날짜보다 하루 전으로 입력한다.
        self.engine_JB.execute(sql % str(int(self.date)-1))

        print("mysql//jacknot1_imi1.setting_all_item 에 daily_crawler 업데이트 완료 여부 표시를, 삭제 날짜의 하루 이전으로 변경합니다.")
        sql="update stock_item_all set check_daily_crawler = 0"
        self.engine_daily_buy_list.execute(sql)


        # 테스트용 코딩
        # sql = "delete from bot_test1.%s where number = %s"
        # self.engine_daily_craw.execute(sql % ('class1', 1))

    # 수정하면서 삭제한 코드
    # def update_check(self):
    #     print("각 종목 테이블이 마지막 날짜까지 업데이트가 되었는지 확인합니다.")
    #     a=0
    #     for i in self.rows:
    #         #mySQL 에서 테이블 이름에 아래 기호들이 들어있으면 양쪽에 ``기호를 추가하기 때문에 넣어주는 조건문.
    #         if '&' in i[0] or '-' in i[0] or ' ' in i[0]:
    #             name="`%s`" %i[0]
    #         else:
    #             name=i[0]
    #         sql = "select date from %s where `index` = 0"
    #         date = self.engine_daily_craw.execute(sql % name).fetchall()
    #         if date[0][0]=="20200825":
    #             date_exist=1
    #         else:
    #             date_exist = 0
    #             print(i, date_exist)
    #             a+=1
    #
    #     if a==0:
    #         print("모든 종목이 마지막 날짜까지 제대로 업데이트 되었습니다.")
    #         return True
    #     else:
    #         print("위에 해당하는 종목들의 업데이트가 미완료 상태입니다.")
    #         return False



print("delete today.py 의 __name__ 은?: ", __name__)
if __name__ == "__main__":
    print("__main__에 들어왔습니다.")
    print("Delete_today 를 생성합니다.")
    dt=Delete_day()
    dt.delete_day_info()
    dt.update_indicator()