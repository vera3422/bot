from library.daily_crawler import *


class Delete_day:
    def __init__(self):
        self.__creat_engine()

    def __creat_engine(self):
        self.engine_daily_craw = create_engine(
            "mysql+mysqldb://" + cf.db_id + ":" + cf.db_passwd + "@" + cf.db_ip + ":" + cf.db_port + "/daily_craw",
            encoding='utf-8')

    def item_tables(self):
        print("daily-craw 폴더 안의 종목 테이블 이름의 list를 불러옵니다.")
        sql="show tables in daily_craw"
        self.rows = self.engine_daily_craw.execute(sql).fetchall()

    def delete_day_info(self):
        print("실행하려면 마지막 코드(def delete_day_info)를 활성화 시키십시오.")
        # 실제 원하는 날 데이터 삭제 코드
        # self.date = input("어떤 일자 데이터를 지우고 싶으신가요? (ex/20201231) : ")
        # print("daily_craw 폴더의 각 종목 테이블에 대하여, %s 에 해당하는 데이터 행을 삭제합니다." %self.date)
        # for i in self.rows:
        #     sql = "delete from daily_craw.%s where date = %s"
        #     self.engine_daily_craw.execute(sql % (i[0], self.date))

        # 테스트용 코딩
        # sql = "delete from bot_test1.%s where number = %s"
        # self.engine_daily_craw.execute(sql % ('class1', 1))

    def update_check(self):
        print("각 종목 테이블이 마지막 날짜까지 업데이트가 되었는지 확인합니다.")
        a=0
        for i in self.rows:
            #mySQL 에서 테이블 이름에 아래 기호들이 들어있으면 양쪽에 ``기호를 추가하기 때문에 넣어주는 조건문.
            if '&' in i[0] or '-' in i[0] or ' ' in i[0]:
                name="`%s`" %i[0]
            else:
                name=i[0]
            sql = "select date from %s where `index` = 0"
            date = self.engine_daily_craw.execute(sql % name).fetchall()
            if date[0][0]=="20200825":
                date_exist=1
            else:
                date_exist = 0
                print(i, date_exist)
                a+=1

        if a==0:
            print("모든 종목이 마지막 날짜까지 제대로 업데이트 되었습니다.")
            return True
        else:
            print("위에 해당하는 종목들의 업데이트가 미완료 상태입니다.")
            return False



print("delete today.py 의 __name__ 은?: ", __name__)
if __name__ == "__main__":
    print("__main__에 들어왔습니다.")
    print("Delete_today 를 생성합니다.")
    dt=Delete_day()
    dt.item_tables()
    if dt.update_check():
        dt.delete_day_info()